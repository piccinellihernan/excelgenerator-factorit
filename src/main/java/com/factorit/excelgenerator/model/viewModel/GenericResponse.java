package com.factorit.excelgenerator.model.viewModel;


public class GenericResponse {
    private String message;
    private String description;

    public GenericResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
