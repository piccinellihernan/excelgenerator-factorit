package com.factorit.excelgenerator.model;

import javax.xml.bind.annotation.*;


@XmlRootElement(name = "Movimiento")
@XmlAccessorType(XmlAccessType.FIELD)
public class Movement {

    @XmlElement(name="SaldoCtaCte")
    private double currentAccountBalance;
    @XmlElement(name="Tipo")
    private String type;
    @XmlElement(name="CodigoMovimiento")
    private int movementCode;
    @XmlElement(name="Concepto")
    private String concept;
    @XmlElement(name="Importe")
    private double amount;

    public double getCurrentAccountBalance() {
        return currentAccountBalance;
    }

    public void setCurrentAccountBalance(double currentAccountBalance) { this.currentAccountBalance = currentAccountBalance; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMovementCode() {
        return movementCode;
    }

    public void setMovementCode(int movementCode) {
        this.movementCode = movementCode;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}