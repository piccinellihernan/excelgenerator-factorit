package com.factorit.excelgenerator.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement(name="Movimientos")
@XmlAccessorType(XmlAccessType.FIELD)
public class Movements {
    @XmlElement(name="Movimiento")
    private List<Movement> movements;

    public List<Movement> getMovements() {
        return movements;
    }

    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

}

