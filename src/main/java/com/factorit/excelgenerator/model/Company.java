package com.factorit.excelgenerator.model;

import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Empresa")
@XmlAccessorType(XmlAccessType.FIELD)
public class Company {
    @XmlElement(name="NroContrato")
    private int contractNumber;
    @XmlElement(name="CUIT")
    private String cuit;
    @XmlElement(name="Denominacion")
    private String denomination;
    @XmlElement(name="Domicilio")
    private String address;
    @XmlElement(name="CodigoPostal")
    private int postalCode;
    @XmlElement(name="FechaDesdeNov")
    private Date dateFrom;
    @XmlElement(name="FechaHastaNov")
    private Date dateTo;
    @XmlElement(name="Organizador")
    private int organizer;
    @XmlElement(name="Productor")
    private int producer;
    @XmlElement(name="CIIU")
    private int ciiu;
    @XmlElement(name="Movimientos")
    private Movements movements;

    public int getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(int contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int codigoPostal) {
        this.postalCode = postalCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getOrganizer() {
        return organizer;
    }

    public void setOrganizer(int organizer) {
        this.organizer = organizer;
    }

    public int getProducer() {
        return producer;
    }

    public void setProducer(int producer) {
        this.producer = producer;
    }

    public int getCiiu() {
        return ciiu;
    }

    public void setCiuu(int ciiu) {
        this.ciiu = ciiu;
    }

    public void setMovements(Movements movements) {
        this.movements = movements;
    }

    public List<Movement> getMovements(){
        return movements.getMovements();
    }

}


