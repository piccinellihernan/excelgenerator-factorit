package com.factorit.excelgenerator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.function.Predicate;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.factorit.excelgenerator.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(info())
                ;
    }

    private ApiInfo info() {

        Contact contact = new Contact("Hernan Piccinelli", "https://www.linkedin.com/in/hernan-piccinelli-6402a7205/", "piccinellihernan@gmail.com");
        return new ApiInfoBuilder()
                .title("ExcelGenerator")
                .description("Challenge desarrollado para FactorIt ")
                .version("1.0.0")
                .license("Apache 2.0")
                .contact(contact)
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                .build();
    }
}
