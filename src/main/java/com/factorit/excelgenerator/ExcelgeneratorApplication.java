package com.factorit.excelgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelgeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelgeneratorApplication.class, args);
    }

}
