package com.factorit.excelgenerator.service.impl;

import com.factorit.excelgenerator.exception.CreateFileException;
import com.factorit.excelgenerator.exception.InvalidExtensionFileException;
import com.factorit.excelgenerator.exception.XmlParserException;
import com.factorit.excelgenerator.model.Companies;
import com.factorit.excelgenerator.service.XmlParserService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

@Service
public class XmlParserServiceImpl implements XmlParserService {
    @Override
    public Companies xmlParserToCompanies(MultipartFile multipartFile) throws  XmlParserException {
        //Parseo el objeto multipartFile a File
        File file = multipartFileToFile(multipartFile);

        try {
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema companiesSchema = sf.newSchema(new File("validation.xsd"));

        JAXBContext jaxbContext = JAXBContext.newInstance(Companies.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbUnmarshaller.setSchema(companiesSchema);

            //Parseo el archivo .xml a un objeto Companies
            Companies companies = (Companies) jaxbUnmarshaller.unmarshal(file);

            return companies;

        } catch (UnmarshalException ue) {
            throw new XmlParserException();
        } catch (JAXBException je) {
            throw new XmlParserException();
        } catch (SAXException e) {
            throw new XmlParserException();
        }


    }

    @Override
    public File multipartFileToFile(MultipartFile multipartFile) throws InvalidExtensionFileException, CreateFileException {
        //Parseo el objeto multipartFile a File
        String fileName = multipartFile.getOriginalFilename();
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        if(!prefix.equals(".xml")){
            throw new InvalidExtensionFileException();
        }
        File file = null;
        try {
            file = File.createTempFile(fileName, prefix);
            multipartFile.transferTo(file);
        } catch (Exception e) {
            throw new CreateFileException();
        }
        return file;

    }
}
