package com.factorit.excelgenerator.service.impl;

import com.factorit.excelgenerator.exception.CreateFileException;
import com.factorit.excelgenerator.model.Companies;
import com.factorit.excelgenerator.service.ExcelGeneratorService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Service
public class ExcelGeneratorServiceImpl implements ExcelGeneratorService {
    @Override
    public ByteArrayResource excelGenerator(Companies companies)  {
        //Obtengo un workbook con la estructura solicitada
        Workbook workbook = workBookCreator();

       Sheet sheetCompanies = workbook.getSheet("Empresas");
       Sheet sheetMovements = workbook.getSheet("Movimientos");

       //Cargo los datos en el sheet de compañias
       companies.getCompanies().forEach(company ->{
           Row rowCompanies = sheetCompanies.createRow(sheetCompanies.getLastRowNum() + 1);
           rowCompanies.createCell(0).setCellValue(company.getContractNumber());
           rowCompanies.createCell(1).setCellValue(company.getCuit());
           rowCompanies.createCell(2).setCellValue(company.getDenomination());
           rowCompanies.createCell(3).setCellValue(company.getAddress());
           rowCompanies.createCell(4).setCellValue(company.getPostalCode());
           rowCompanies.createCell(5).setCellValue(company.getProducer());
           //Cargo los datos en el sheet de movimientos
           company.getMovements().forEach(movement -> {
                Row rowMovement = sheetMovements.createRow(sheetMovements.getLastRowNum() + 1);
                rowMovement.createCell(0).setCellValue(company.getContractNumber());
                rowMovement.createCell(1).setCellValue(movement.getCurrentAccountBalance());
                rowMovement.createCell(2).setCellValue(movement.getConcept());
                rowMovement.createCell(3).setCellValue(movement.getAmount());
            });
       });


        try {
            //Creo un archivo temporal con formato .xlsx
            File currDir = File.createTempFile("temp",".xml");
            FileOutputStream  outputStream = new FileOutputStream(currDir.getAbsolutePath());

            //Escribo el archivo
            workbook.write(outputStream);
            workbook.close();

            Path path = Paths.get(currDir.getAbsolutePath());
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            return resource;

        }
        catch (FileNotFoundException e) {
              throw new CreateFileException();
        }
        catch (IOException e) {
              throw new CreateFileException();
    }

}
        @Override
        public Workbook workBookCreator() {
        //Genero un workbook con el formato requerido
        Workbook workbook = new XSSFWorkbook();
        Sheet sheetCompanies = workbook.createSheet("Empresas");
        Row headerCompanies = sheetCompanies.createRow(0);
        CellStyle headerStyle = workbook.createCellStyle();

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);
        List<String> cellCompanies = Arrays.asList("NroContrato", "Cuit", "Denominacion", "Domicilio", "Codigo Postal", "Productor");
        List<Integer> cellCompaniesSize = Arrays.asList(6000,6000,6000,12000,6000,6000);


        IntStream.range(0, cellCompanies.size()).forEach(index -> {
            Cell headerCellCompanies = headerCompanies.createCell(index);
            headerCellCompanies.setCellValue(cellCompanies.get(index));
            headerCellCompanies.setCellStyle(headerStyle);
            sheetCompanies.setColumnWidth(index, cellCompaniesSize.get(index));

        });

        Sheet sheetMovements = workbook.createSheet("Movimientos");
        Row headerMovements = sheetMovements.createRow(0);
        List<String> cellsMovements = Arrays.asList("NroContrato", "SaldoCtaCte", "Concepto", "Importe");
        List<Integer> cellsMovementsSize = Arrays.asList(6000,6000,6000,6000);

        IntStream.range(0, cellsMovements.size()).forEach(index -> {
            Cell headerCellMovements = headerMovements.createCell(index);
            headerCellMovements.setCellValue(cellsMovements.get(index));
            headerCellMovements.setCellStyle(headerStyle);
            sheetMovements.setColumnWidth(index, cellsMovementsSize.get(index));
        });

        return workbook;
    }


}
