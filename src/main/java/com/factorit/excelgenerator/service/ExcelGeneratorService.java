package com.factorit.excelgenerator.service;

import com.factorit.excelgenerator.model.Companies;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.ByteArrayResource;



public interface ExcelGeneratorService {
public ByteArrayResource excelGenerator(Companies companies);
public Workbook workBookCreator();

}
