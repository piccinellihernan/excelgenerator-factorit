package com.factorit.excelgenerator.service;

import com.factorit.excelgenerator.model.Companies;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface XmlParserService {
    public Companies xmlParserToCompanies(MultipartFile multipartFile);
    public File multipartFileToFile (MultipartFile multipartFile);
}
