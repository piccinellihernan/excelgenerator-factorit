package com.factorit.excelgenerator.exception;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateFileException extends RuntimeException {
    private String errorMessage;
}
