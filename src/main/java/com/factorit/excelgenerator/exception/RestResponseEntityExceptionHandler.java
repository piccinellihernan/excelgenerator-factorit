package com.factorit.excelgenerator.exception;


import com.factorit.excelgenerator.model.viewModel.GenericResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {



    public ResponseEntity<Object> buildGeneric(final RuntimeException ex,
                                               final WebRequest request, String error, String description, HttpStatus status) {
        final GenericResponse response = new GenericResponse();
                    response.setDescription(description);
                    response.setMessage(error);
        return  handleExceptionInternal(ex, response, new HttpHeaders(), status, request);
    }

    @ExceptionHandler({ XmlParserException.class })
    public ResponseEntity<Object> handleXmlParserException(final RuntimeException ex, final WebRequest request) {
        return buildGeneric(ex, request, "Xml parser error",
                "Use a valid .xml file", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ InvalidExtensionFileException.class })
    public ResponseEntity<Object> handleInvalidExtensionFileException(final RuntimeException ex, final WebRequest request) {
        return buildGeneric(ex, request, "Wrong file extension",
                "File extension must be .xml", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ CreateFileException.class })
    public ResponseEntity<Object> handleCreateFileException(final RuntimeException ex, final WebRequest request) {
        return buildGeneric(ex, request, "File error",
                "Error creating file", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ Exception.class, NullPointerException.class})
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        return buildGeneric(ex, request, "Internal Server Error",
                "An internal server error has occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
