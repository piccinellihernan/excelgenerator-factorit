package com.factorit.excelgenerator.controller;

import com.factorit.excelgenerator.model.Companies;
import com.factorit.excelgenerator.service.ExcelGeneratorService;
import com.factorit.excelgenerator.service.XmlParserService;
import com.factorit.excelgenerator.utils.Utils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;


@Controller
@RequestMapping("/")
public class MainController {

    @Autowired XmlParserService XmlParserServiceImpl;
    @Autowired ExcelGeneratorService ExcelGeneratorServiceImpl;

    @PostMapping("/api/download")
    @ApiOperation("Recieve an Xml file with a list of companies and returns an excel file with that data")
    public @ResponseBody
    ResponseEntity<Resource> submit(@RequestParam("file") MultipartFile multipartFile) {

        //Parseo el archivo .xml en un objeto Companies
        Companies companies = XmlParserServiceImpl.xmlParserToCompanies(multipartFile);

        //Con los datos almacenados en companies genero el excel
        ByteArrayResource resource = ExcelGeneratorServiceImpl.excelGenerator(companies);

        Utils utils = new Utils();

        return ResponseEntity.ok()
                .headers(utils.getHeaders())
                .contentLength(resource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);


    }



    @GetMapping("/upload")
    @ApiOperation("Formulary for xml file upload")
    public String upload() {
        //Retorno la vista con el formulario para cargar el archivo xml
        return "upload";
    }
}
