package com.factorit.excelgenerator;


import com.factorit.excelgenerator.exception.InvalidExtensionFileException;
import com.factorit.excelgenerator.exception.XmlParserException;
import com.factorit.excelgenerator.model.Companies;
import com.factorit.excelgenerator.service.XmlParserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;


import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest



public class XmlParserServiceImplTest {

    String xml = "<Empresas>\n" +
            "  <Empresa>\n" +
            "    <NroContrato>1</NroContrato>\n" +
            "    <CUIT>111111111111</CUIT>\n" +
            "    <Denominacion>Prueba 1</Denominacion>\n" +
            "    <Domicilio>ILDEFONSO MUÑECAS 167 - B° CENTRO</Domicilio>\n" +
            "    <CodigoPostal>4000</CodigoPostal>\n" +
            "    <FechaDesdeNov>2021-01-13T00:00:00</FechaDesdeNov>\n" +
            "    <FechaHastaNov>2021-01-14T00:00:00</FechaHastaNov>\n" +
            "    <Organizador>2271</Organizador>\n" +
            "    <Productor>26672</Productor>\n" +
            "    <CIIU>623016</CIIU>\n" +
            "    <Movimientos>\n" +
            "      <Movimiento>\n" +
            "\t    <SaldoCtaCte>-21802.12</SaldoCtaCte>\n" +
            "        <Tipo>C</Tipo>\n" +
            "        <CodigoMovimiento>1</CodigoMovimiento>\n" +
            "        <Concepto>Pago</Concepto>\n" +
            "        <Importe>21802.19</Importe>\n" +
            "      </Movimiento>\n" +
            "    </Movimientos>\n" +
            "  </Empresa>\n" +
            "</Empresas>";

    MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
    MockMultipartFile secondFile = new MockMultipartFile("xml", "other-file-name.xml", "text/plain", "some other type".getBytes());
    MockMultipartFile thirdFile = new MockMultipartFile("xml", "other-file-name.xml", "xml", xml.getBytes());



    @Autowired
    private XmlParserService XmlParserServiceImpl;

    @Test( expected = InvalidExtensionFileException.class )
    public void whenMultipartFileToFile_ThenInvalidExtensionFileException(){
        XmlParserServiceImpl.multipartFileToFile(firstFile);
    }


    @Test( expected = XmlParserException.class )
    public void whenXmlParserToCompanies_thenXmlParserException(){
        XmlParserServiceImpl.xmlParserToCompanies(secondFile);
    }

    @Test
    public void whenXmlParserToCompanies_thenReturnCompaniesObj(){
        Companies companies = XmlParserServiceImpl.xmlParserToCompanies(thirdFile);
        assertThat(companies).isNotNull();
    }


}
