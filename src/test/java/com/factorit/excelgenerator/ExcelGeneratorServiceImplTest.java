package com.factorit.excelgenerator;


import com.factorit.excelgenerator.model.Companies;
import com.factorit.excelgenerator.service.ExcelGeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExcelGeneratorServiceImplTest {

    Companies companies1 = new Companies();

    @Autowired
    private ExcelGeneratorService ExcelGeneratorServiceImpl;

    @Test( expected = NullPointerException.class )
    public void whenExcelGenerator_NullPointerException(){
        ExcelGeneratorServiceImpl.excelGenerator(companies1);
    }


}
